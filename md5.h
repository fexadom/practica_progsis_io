#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct {
    uint8_t b[16];
} md5_hash_t;

md5_hash_t md5(uint8_t *initial_msg, size_t initial_len);