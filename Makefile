CFLAGS=-g -lm

.PHONY: all
all: check_md5

check_md5: check_md5.c md5.c
	gcc $(CFLAGS) -o check_md5 check_md5.c md5.c

.PHONY: clean
clean:
	rm check_md5
