#include "md5.h"

int main(int argc, char **argv)
{
	char *mensaje;
	int s;

	if (argc != 2) {
		fprintf(stderr, "uso: %s <mensaje>\n", argv[0]);
		return 0;
	}

	mensaje = argv[1];
	s = strlen(mensaje);

	printf("El mensaje es: %s y tiene %d caracteres\n", mensaje, s);

	md5_hash_t h = md5(mensaje,s);

	printf("El Hash MD5 del mensaje es: ");
	for(int i=0;i<16;i++) {
		printf("%2.2x",h.b[i]);
	}
	printf("\n");

	return 0;
}